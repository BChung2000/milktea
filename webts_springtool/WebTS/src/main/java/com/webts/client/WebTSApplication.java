package com.webts.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebTSApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebTSApplication.class, args);
	}

}
