package com.webts.client;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MVCConfig implements WebMvcConfigurer {

	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		System.out.println("MVCConfig::addViewControllers");
		registry.addViewController("/login").setViewName("login");
		//registry.addViewController("/register").setViewName("signup");
	}
}
