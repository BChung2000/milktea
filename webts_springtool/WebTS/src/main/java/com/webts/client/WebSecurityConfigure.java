package com.webts.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import com.webts.client.handler.OAuthenticationSuccess;
import com.webts.client.oauth2.CustomOauth2UserService;

@Configuration
@EnableWebSecurity
public class WebSecurityConfigure extends WebSecurityConfigurerAdapter {

	@Autowired
	private CustomOauth2UserService userService;
	
	@Autowired
	private OAuthenticationSuccess oauthenticationSuccess;
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
			.antMatchers("/", "/register", "/oauth2/**", "/assets/**", "/login_assets/**").permitAll()
			.anyRequest().authenticated()
			.and()
			//.formLogin().loginPage("/login").permitAll().and()
			.oauth2Login().loginPage("/login").permitAll()
			.userInfoEndpoint().userService(userService)
			.and().successHandler(oauthenticationSuccess)
			.and()
			.logout().permitAll()
			;
	}
}
