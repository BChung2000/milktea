package com.webts.client.helpers;

public enum AuthProvider {
	BASIC,
	FACEBOOK,
	GOOGLE
}
