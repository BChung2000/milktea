package com.webts.client.handler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import com.webts.client.helpers.AuthProvider;
import com.webts.client.models.Customer;
import com.webts.client.oauth2.CustomOauth2User;
import com.webts.client.services.CustomerServices;

@Component
public class OAuthenticationSuccess extends
				SimpleUrlAuthenticationSuccessHandler {
	
	@Autowired
	private CustomerServices customerServices;
	
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {
		
		CustomOauth2User oauth2User = (CustomOauth2User) authentication.getPrincipal();
		String email = oauth2User.getEmail();
		String fullName = oauth2User.getName();
		String servletPath = request.getServletPath();
		
		System.out.println("Customer logged in by email: " + email);
		System.out.println("Customer name is: " + fullName);
		System.out.println("Customer logged server path: " + servletPath);
		
		AuthProvider provider = AuthProvider.BASIC;
		
		if (servletPath.contains("facebook")) {
			provider = AuthProvider.FACEBOOK;
		} else if (servletPath.contains("google")) {
			provider = AuthProvider.GOOGLE;
		}
		
		Customer customer = customerServices.getByEmail(email);
		
		if (customer == null) {
			customerServices.registerNewCusomer(email, fullName, provider);
		} else {
			customerServices.updateCustomer(customer, fullName, provider);
		}
		
		super.onAuthenticationSuccess(request, response, authentication);
	}
}
