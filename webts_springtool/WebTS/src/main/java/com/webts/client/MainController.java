package com.webts.client;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.webts.client.helpers.AuthProvider;
import com.webts.client.models.Customer;
import com.webts.client.services.CustomerServices;

@Controller
public class MainController {

	@Autowired
	private CustomerServices customerServices;
	
	@GetMapping("/register")
	public String showRegister(Model model) {
		Customer customer = new Customer();
		
		model.addAttribute("customer", customer);
		
		return "signup";
	}
	
	@PostMapping("/register")
	public String postRegister(@Valid @ModelAttribute("customer") Customer customer,
			BindingResult bindingResult) {
		System.out.println("postRegister Email: " + customer.getEmail());
		System.out.println("postRegister Password: " + customer.getPassword());
		
		if (bindingResult.hasErrors()) {
			return "signup";
		}
		
		Customer currentCustomer = customerServices.getByEmail(customer.getEmail());
		
		if (currentCustomer == null) {
			customerServices.registerNewCustomer(customer, AuthProvider.BASIC);
			return "signup_success";
		}
		
		return "signup_failed";
	}
}
