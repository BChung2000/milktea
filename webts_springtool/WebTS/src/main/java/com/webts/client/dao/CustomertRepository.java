package com.webts.client.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.webts.client.models.Customer;

public interface CustomertRepository extends JpaRepository<Customer, Integer> {

	@Query("SELECT customer FROM Customer customer WHERE customer.email = :email")
	public Customer getByEmail(@Param("email") String email);
}
